### 详解__new__() 和__init__()
---
#### TLDR
```
__new__(cls, *args, **kwargs)
__init__(self, *args, **kwargs)
```
> `cls`为类对象本身, `self`为类实例对象， `__new__()`接收cls创建cls类的实例，再由`__init__()`接收参数为创建的实例初始化

#### 解释
python中一切皆为对象，python类本身也是一种对象，我们可以称其为类对象。对象=属性+方法，对象是类的实例，准确地来说，应该是：实例对象是类对象的实例。 

类主要是定义对象的结构，然后我们以类为模板创建对象。类不但包含方法定义，还包含所有实例共享的数据。  

大家应该对`__init__()`方法都很熟悉，它的第一个参数一定是self,`__init__()`方法负责对象的初始化，系统执行该方法前，其实该实例对象已经存在，要不然初始化什么呢，

先看一小段代码：
```python
class Dog():
    def __new__(cls, *args, **kwargs):
        print("run the new of dog")
        #return super(Dog,cls).__new__(cls)
        return object.__new__(cls)  #两条return语句作用相同

    def __init__(self):
        print("run the init of dog")
        print(self)
        print(self.__class__)

a = Dog()
# run the new of dog
# run the init of dog
# <__main__.Dog object at 0x00000197AAA3A8D0>
# <class '__main__.Dog'>

作者：Obsession
链接：https://zhuanlan.zhihu.com/p/261579683
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
```

可以看出，
1. 当我实例化Dog类对象时，python中首先调用的是类对象的`__new__()`方法，如果该对象没有定义`__new__()`方法，则去父类中依次查找，直到`object`类（`object`类是所有类的基类哦）。
2. `__new__()`的返回语句中，`object.__new__(cls)`意思是调用父类（`object`）的`__new__()`，`super()`是一个特殊函数，帮助python将父类和子类关联起来，父类也成超类，名称`super`因此得名。
3. `__new__()`需要传递一个参数`cls`，`__init__()`需要传递一个参数`self`，`self`代表的就是实例对象本身，`cls`代表的是类对象本身。python中的`self`相当于C++的`this`指针。`__new__()`必须要有返回值，返回实例化出来的实例对象。
4. 看一下阿里云天池python训练营对这两个魔法方法的教学：

> `__new__(cls[, ...])` 是在一个对象实例化的时候所调用的第一个方法，在调用`__init__`初始化前，先调用`__new__`。
`__new__`至少要有一个参数`cls`，代表要实例化的类，此参数在实例化时由 Python 解释器自动提供，后面的参数直接传递给`__init__`。
`__new__`对当前类进行了实例化，并将实例返回，传给`__init__`的`self`。但是，执行了`__new__`，并不一定会进入`__init__`，只有`__new__`返回了，当前类`cls`的实例，当前类的`__init__`才会进入。
若`__new__`没有正确返回当前类`cls`的实例，那`__init__`是不会被调用的，即使是父类的实例也不行，将没有`__init__`被调用。
`__new__`方法主要是当你继承一些不可变的 `class` 时（比如`int`, `str`, `tuple`）， 提供给你一个自定义这些类的实例化过程的途径。

再来看几个例子。

通常来说，类开始实例化时，`__new__()`方法会返回`cls`（`cls`指代当前类）的实例，然后该类的`__init__()`方法会接收这个示例（即`self`）作为自己的第一个参数，然后依次转入`__new__()`方法中接收的位置参数和命名参数。

notice：如果`__new__()`没有返回`cls`（即当前类的实例），那么当前类的`__init__()`方法是不会被调用的，如果`__new__()`返回了其他类的实例，那么只会调用被返回的那个类的构造方法。

上代码：
```python

class A(object):
    def __init__(self, *args, **kwargs):
        print("run the init of A")
    def __new__(cls, *args, **kwargs):
        print("run thr new of A")
        return object.__new__(B, *args, **kwargs)

class B(object):
    def __init__(self):
        print("run the init of B")
    def __new__(cls, *args, **kwargs):
        print("run the new of B")
        return object.__new__(cls)

a = A()
print(type(a))
# run thr new of A
# <class '__main__.B'>
b = B()
print(type(b))
# run the new of B
# run the init of B
# <class '__main__.B'>
```

接下来，看一下单例模式。

1. 单例模式是什么

举个常见的单例模式例子，我们日常使用的电脑上都有一个回收站，在整个操作系统中，回收站只能有一个实例，整个系统都使用这个唯一的实例，而且回收站自行提供自己的实例。因此回收站是单例模式的应用。

确保某一个类只有一个实例，而且自行实例化并向整个系统提供这个实例，这个类称为单例类，单例模式是一种对象创建型模式。
2. 创建单例-保证只有一个对象
```python

# 实例化一个单例
class Singleton(object):
    __instance = None

    def __new__(cls, age, name):
        #如果类数字__instance没有或者没有赋值
        #那么就创建一个对象，并且赋值为这个对象的引用，保证下次调用这个方法时
        #能够知道之前已经创建过对象了，这样就保证了只有1个对象
        if not cls.__instance:
            cls.__instance = object.__new__(cls)
        return cls.__instance
    
    def __init__(self,age,name):
        self.age = age
        self.name = name
a = Singleton(18, "wk")
b = Singleton(8, "mm")

print(id(a)==id(b))
print(a.age,a.name)
print(b.age,b.name)
a.size = 19 #给a指向的对象添加一个属性
print(b.size)#获取b指向的对象的age属性

# True
# 8 mm
# 8 mm
# 19
```
3. 创建单例，只执行一次__init__()方法。
```python

# 实例化一个单例
class Singleton(object):
    __instance = None
    __first_init = False

    def __new__(cls, age, name):
        if not cls.__instance:
            cls.__instance = object.__new__(cls)
        return cls.__instance

    def __init__(self,age,name):
        if not self.__first_init:
            self.age = age
            self.name = name
            Singleton.__first_init = True

a = Singleton(18, "wk")
b = Singleton(8, "mm")

print(id(a)==id(b))
print(a.age,a.name)
print(b.age,b.name)
a.size = 19 #给a指向的对象添加一个属性
print(b.size)#获取b指向的对象的age属性

# True
# 18 wk
# 18 wk
# 19
```

总结下`__init__()`和`__new__()`的区别：
1. `__init__()`通常用于初始化一个新实例，控制这个初始化的过程，比如添加一些属性，做一些额外的操作，发生在类实例被创建完以后。它是实例级别的方法。
2. `__new__()`通常用于控制生成一个新实例的过程。它是类级别的方法。
3. `__new__()`至少有一个参数`cls`，代表要实例化的类，此参数在实例化时会有python编辑器自动提供。
4. `__new__()`必须有返回值，返回实例化出来的实例。
5. 如果将类比作制造商，`__new__()`方法发就是前期的原材料环节，`__init__()`方法就是在有了原材料的基础上，加工，初始化商品的环节。
