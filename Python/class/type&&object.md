Python中一切皆是对象，类可以实例化为实例，同时类也是metaclass的实例化

---
python中类的概念，是借鉴自smalltalk语言。

在大部分语言中，类指的是"描述如何产生一个对象(object)"的一段代码，这对于python也是如此。  

但是,在python中，类远不止如此，类同时也是对象。

当你遇到关键词class的时候，python就会自动执行产生一个对象。下面的代码段中:
```python
>>> class ObjectCreator(object):
         pass
```
python在内存中产生了一个名叫做"ObjectCreator"的对象。这个对象(类)自身拥有产生对象(实例instance)的能力。 这就是为什么称呼这东西(后面遇到容易混淆的地方,我们称之为:类对象)也是类的原因。同时，它也是一个对象，因此你可以对它做如下操作:
- 赋值给变量
- 复制它
- 为它增加属性(attribute)
- 作为参数传值给函数  

举例：
```python
>>> print(ObjectCreator) # 你可以打印一个类,因为它同时也是对象
<class '__main__.ObjectCreator'>

>>> def echo(o):
...     print(o)
...
>>> echo(ObjectCreator) # 作为参数传值给函数
<class '__main__.ObjectCreator'>

>>> print(hasattr(ObjectCreator, 'new_attribute'))
False
>>> ObjectCreator.new_attribute = 'foo' # you can add attributes to a class
>>> print(hasattr(ObjectCreator, 'new_attribute'))
True
>>> print(ObjectCreator.new_attribute)
foo

>>> ObjectCreatorMirror = ObjectCreator # 将类赋值给变量
>>> print(ObjectCreatorMirror.new_attribute)
foo
>>> print(ObjectCreatorMirror())
<__main__.ObjectCreator object at 0x8997b4c>
```
## 动态创建类
---
既然类也是对象，那就应该有用来产生它的东西。这东西就是type。

先来说说你所认识的type。这个古老而好用的函数，可以让我们知道一个对象的类型是什么。

```python
>>> print(type(1))
<type 'int'>
>>> print(type("1"))
<type 'str'>
>>> print(type(ObjectCreator))
<type 'type'>
>>> print(type(ObjectCreator()))
<class '__main__.ObjectCreator'>
```

实际上，type还有一个完全不同的功能，它可以在运行时产生类。type可以传入一些参数，然后返回一个类。(好吧，必须承认，根据不同的传入参数，一个相同的函数type居然会有两个完全不同的作用，这很愚蠢。不过python这样做是为了保持向后兼容性。)

下面举例type创建类的用法。首先，对于类一般是这么定义的:

```python
>>> class MyShinyClass(object):
        pass
```
在下面，MyShinyClass也可以这样子被创建出来,并且跟上面的创建方法有一样的表现:
```
>>> MyShinyClass = type('MyShinyClass', (), {}) # returns a class object
>>> print(MyShinyClass)
<class '__main__.MyShinyClass'>
>>> print(MyShinyClass()) # create an instance with the class
<__main__.MyShinyClass object at 0x8997cec>
```
type创建类需要传入三个参数,分别为name、bases、dict:

- 类的名字
- 一组"类的父类"的元组(tuple) (这个会实现继承,也可以为空)
- 字典 (类的属性名与值,key-value的形式，不传相当于为空，如一般写法中的pass).

下面来点复杂的，来更好的理解type传入的三个参数:
```python
class Foo(object):
    bar = True

    def echo_bar(self):
        print(self.bar)
```
等价于：
```python 
def echo_bar(self):
    print(self.bar)

Foo = type('Foo', (object,), {'bar':True, 'echo_bar': echo_bar})
# python 元组当只有1个元素时，不可省略逗号
```
```python
class FooChild(Foo):
    pass
```
等价于：
```python
FooChild = type('FooChild', (Foo, ), {})
```
在python中，类就是对象，并且你可以在运行的时候动态创建类.
## Metaclass
很多书都会翻译成 元类，仅从字面理解， meta 的确是元，本源，翻译没毛病。但理解时，应该把元理解为描述数据的超越数据，事实上，metaclass 的 meta 起源于希腊词汇 meta，包含两种意思：

- “Beyond”，例如技术词汇 metadata，意思是描述数据的超越数据。
- “Change”，例如技术词汇 metamorphosis，意思是改变的形态。
因此可以理解为 metaclass 为描述类的超类，同时可以<u>改变子类的形态</u>。



**Thomas' description of meta-classes here is excellent:**
> A metaclass is the class of a class. Like a class defines how an instance of the class behaves, a metaclass defines how a class behaves. A class is an instance of a metaclass.
---
metaclass 就是创建类的那家伙。(事实上，type就是一个metaclass)

我们知道,我们定义了class就是为了能够创建object的，没错吧?

我们也学习了，python中类也是对象。

那么，metaclass就是用来创造“类对象”的类.它是“类对象”的“类”。

可以这样子来理解:
![metaclass](./img1.png)
python在背后使用type创造了所有的类。type是所有类的metaclass.

我们可以使用__class__属性来验证这个说法.

在python中，一切皆为对象：整数、字符串、函数、类.所有这些对象，都是通过类来创造的.
```python
>>> age = 35
>>> age.__class__
<type 'int'>

>>> name = 'bob'
>>> name.__class__
<type 'str'>

>>> def foo(): pass
>>> foo.__class__
<type 'function'>

>>> class Bar(object): pass
>>> b = Bar()
>>> b.__class__
<class '__main__.Bar'>
```
type是python內置的metaclass。不过，你也可以编写自己的metaclass.

我们可以在一个类中加入 metaclass 属性.
```python
class Foo(metaclass=something):
    ......  # 省略
```

当你这么做了，python就会使用metaclass来创造类:Foo。

当你写下class Foo(metaclass)的时候，类对象Foo还没有在内存中生成。

python会在类定义中寻找metaclass 。如果找到了，python就会使用这个metaclass 来创造类对象: Foo。如果没有在Foo中找到metaclass，它会继续在Bar（父类）中寻找metaclass，并尝试做和前面同样的操作。

如果python由下往上遍历父类也都没有找不到metaclass，它就会在模块(module)中去寻找metaclass，并尝试做同样的操作。

如果还是没有找不到metaclass， 才会用内置的type(这也是一个metaclass)来创建这个类对象。

## 自定义metaclass

使用metaclass的主要目的，是为了能够在创建类的时候，自动地修改类。

一个很傻的需求，我们决定要将该模块中的所有类的属性，改为大写。

```python
# type也是一个类，我们可以继承它.
class UpperAttrMetaclass(type):
    # __new__ 是在__init__之前被调用的特殊方法
    # __new__是用来创建对象并返回这个对象
    # 而__init__只是将传入的参数初始化给对象
    # 实际中,你很少会用到__new__，除非你希望能够控制对象的创建
    # 在这里，类是我们要创建的对象，我们希望能够自定义它，所以我们改写了__new__
    # 如果你希望的话，你也可以在__init__中做些事情
    # 还有一些高级的用法会涉及到改写__call__，但这里我们就先不这样.

    def __new__(cls, name,bases, dict):
        uppercase_attr = {}
        for name, val in dict.items():
            if not name.startswith('__'):
                uppercase_attr[name.upper()] = val
            else:
                uppercase_attr[name] = val
        return type(name, bases, uppercase_attr)
```
这里的方式其实不是OOP(面向对象编程).因为我们直接调用了type,而不是改写父类的__type__方法.

所以我们也可以这样子处理:
```python
class UpperAttrMetaclass(type):
    def __new__(cls, name,bases, dict):
        uppercase_attr = {}
        for name, val in dict.items():
            if not name.startswith('__'):
                uppercase_attr[name.upper()] = val
            else:
                uppercase_attr[name] = val
        return type.__new__(cls, name,
                            bases, uppercase_attr)
```
注意到__new__方法相比于type()函数多了一个参数cls(类本身)，即创建了一个cls类

(我们同时使用常见的super来让代码更清晰)
```python
class UpperAttrMetaclass(type):

    def __new__(cls, clsname, bases, attrs):
        uppercase_attr = {}
        for name, val in attrs.items():
            if not name.startswith('__'):
                uppercase_attr[name.upper()] = val
            else:
                uppercase_attr[name] = val
        return super(UpperAttrMetaclass, cls).__new__(cls, clsname, bases, uppercase_attr)
```




### cls and self

> Before diving into the actual implementations you need to know that `__new__` accepts cls as it's first parameter and `__init__` accepts self, because when calling `__new__` you actually don't have an instance yet, therefore no self exists at that moment, whereas `__init__` is called after `__new__` and the instance is in place, so you can use self with it.


### new-style class and old-style class
New-style classes inherit from object, or from another new-style class.
```python
class NewStyleClass(object):
    pass

class AnotherNewStyleClass(NewStyleClass):
    pass
```
Old-style classes don't.
```python
class OldStyleClass():
    pass
```
**Python 3 Note:**

Python 3 doesn't support old style classes, so either form noted above results in a new-style class.
```python
class OldStyleClass():
    pass

#In Python3.X is equal to 

class OldStyleClass(object):
    pass
```

### type and object

![关系图](./img2.png)

```python
#type is kind of object,即Type是object的子类
>>> type.__class__
<class 'type'> # type是type的一个实例，即type是type类
>>> type.__bases__
(<class 'object'>,) # type的父类是object

#object is and instance of type,即Object是type的一个实例
>>> object.__class__
<class 'type'> # object是type的一个实例，即object是type类
>>> object.__bases__ 
() # object 无父类，因为它是链条顶端
```

![类关系](./img3.png)
> 虚线表示源是目标的实例，实线表示源是目标的子类。即，左边的是右边的类型，而上面的是下面的父亲。

---
### difference between `type()` and `type.__new__()`
[链接](https://stackoverflow.com/questions/2608708/what-is-the-difference-between-type-and-type-new-in-python)