# pdb
---
## TLDR
```bash
python -m ipdb demo.py
```
## install
`pip install ipdb`

## 集成到源码
```python
import ipdb
ipdb.set_trace() # 在打断点处 
```
运行到set_trace处后停止，展开ipython环境，进行调试
## 命令式

```bash
python -m ipdb demo.py
```
进入ipython环境如下
```bash
$python -m ipdb code.py
ipdb> b 3
Breakpoint 1 at /test.py:3
ipdb> c
```
## command
`h # 查看可用命令`

```
========================================
EOF    cl         disable  interact  next    psource  rv         unt
a      clear      display  j         p       q        s          until
alias  commands   down     jump      pdef    quit     source     up
args   condition  enable   l         pdoc    r        step       w
b      cont       exit     list      pfile   restart  tbreak     whatis
break  continue   h        ll        pinfo   return   u          where
bt     d          help     longlist  pinfo2  retval   unalias
c      debug      ignore   n         pp      run      undisplay

Miscellaneous help topics:
==========================
exec  pdb
```
full name|alias|describe
:-|:-|:-
args|a|进入函数后显示函数参数
alias||给命令起别名
unalias||删除别称
break|b|打断点
where|bt或w|显示当前堆栈
continue|cont或c|继续运行至下一个断点处
clear|cl|清除断点
down|d|进入下一层堆栈
up|u|进入上一次堆栈
exit或quit|q|退出debugger
tbreak||打临时断点,运行一次后失效
next|n|下一步，不进入函数
step|s|下一步，进入函数（列表生成式等）
until|unt|继续执行
whatis||显示变量类型
longlist|ll|显示代码
list|l|显示代码，不断翻页
condition||条件断点
jump|j|跳转到指定行继续
enable, disable||开关断点
run|restart|重新运行
return|r|执行完当前函数并返回，用于退出step into的函数
retval|rv|显示函数返回值
display, undisplay||显示变化的值
source||显示对象的源码
interact||开启交互模式`Use quit() or Ctrl-Z plus Return to exit`
p||打印
pp||pretty-print
ignore||忽略断点
debug||进入recursive debugger
commands||在断点处设置新命令

---
- a(rgs)
```
     13 if __name__ == '__main__':
1--> 14     f(5)

ipdb> s
--Call--
> d:\markdown\ipdb\demo.py(7)f()
      6 
----> 7 def f(n):
      8     for i in range(n):

ipdb> a
n = 5
```
- alias [name [command [parameter parameter ...] ]]
```
ipdb> alias pi for i in range(%1):print(3.14)
ipdb> pi 3
3.14
3.14
3.14
```
> `%1, %2`表示第1、2个参数， `%*`表示所有参数
- b(reak) [ ([filename:]lineno | function) [, condition] ]
>  第一个参数为函数名时，在函数第一行打断点，第二个参数为表达式，当表达式为True时打断点
> b 不加行号显示所有断点
```
ipdb> b 14
Breakpoint 6 at d:\markdown\ipdb\demo.py:14
ipdb> l 14
      9         j = calc(i, n)
     10         print(i, j)
     11     return
     12 
     13 if __name__ == '__main__':
6    14     f(5)
```
> 最左侧的数字为断点index
```
ipdb> b
Num Type         Disp Enb   Where
6   breakpoint   keep yes   at d:\markdown\ipdb\demo.py:14
7   breakpoint   del  yes   at d:\markdown\ipdb\demo.py:3
```
> 此时共有两个断点
```
ipdb> b 9 ,i==2
Breakpoint 11 at d:\markdown\ipdb\demo.py:9
ipdb> b
Num Type         Disp Enb   Where
11  breakpoint   keep yes   at d:\markdown\ipdb\demo.py:9
        stop only if i==2
ipdb> c
0 0
1 5
> d:\markdown\ipdb\demo.py(9)f()
      8     for i in range(n):
11--> 9         j = calc(i, n)
     10         print(i, j)

ipdb> p i
2
```
> 当`i==2`时触发断点
- w
> 当带有参数时，表示显示的堆栈信息行数
```
ipdb> w
  c:\users\17209\anaconda3\lib\bdb.py(585)run()
    584         try:
--> 585             exec(cmd, globals, locals)
    586         except BdbQuit:

  <string>(1)<module>()

> d:\markdown\ipdb\demo.py(1)<module>()
----> 1 import pdb
      2 
7     3 def calc(i, n):

ipdb> w 5
  c:\users\17209\anaconda3\lib\bdb.py(585)run()
    583         sys.settrace(self.trace_dispatch)
    584         try:
--> 585             exec(cmd, globals, locals)
    586         except BdbQuit:
    587             pass

  <string>(1)<module>()

> d:\markdown\ipdb\demo.py(1)<module>()
----> 1 import pdb
      2 
7     3 def calc(i, n):
      4     j = i * n
      5     return j

ipdb> w 2
  c:\users\17209\anaconda3\lib\bdb.py(585)run()
    584         try:
--> 585             exec(cmd, globals, locals)

  <string>(1)<module>()

> d:\markdown\ipdb\demo.py(1)<module>()
----> 1 import pdb
      2 
```
- cl(ear) filename:lineno | [bpnumber [bpnumber...]]
> 参数为断点index，不带参数清除所有断点
```
ipdb> b
Num Type         Disp Enb   Where
6   breakpoint   keep yes   at d:\markdown\ipdb\demo.py:14
7   breakpoint   del  yes   at d:\markdown\ipdb\demo.py:3
ipdb> cl 7
Deleted breakpoint 7 at d:\markdown\ipdb\demo.py:3
ipdb> b
Num Type         Disp Enb   Where
6   breakpoint   keep yes   at d:\markdown\ipdb\demo.py:14
```
- tbreak [ ([filename:]lineno | function) [, condition] ]
```
ipdb> tbreak 3
Breakpoint 12 at d:\markdown\ipdb\demo.py:3
ipdb> b
Num Type         Disp Enb   Where
12  breakpoint   del  yes   at d:\markdown\ipdb\demo.py:3
```
- whatis arg
```
ipdb> whatis n
<class 'int'>
```
- condition bpnumber [condition]
> 对断点index操作
```
ipdb> b
Num Type         Disp Enb   Where
12  breakpoint   del  yes   at d:\markdown\ipdb\demo.py:3
ipdb> condition 12 i==2
New condition set for breakpoint 12.
ipdb> b
Num Type         Disp Enb   Where
12  breakpoint   del  yes   at d:\markdown\ipdb\demo.py:3
        stop only if i==2
```
- j(ump) lineno
```
ipdb> l
     12 
     13 if __name__ == '__main__':
     14     f(5)

ipdb> j 3
> d:\markdown\ipdb\demo.py(3)<module>()
      2 
12--> 3 def calc(i, n):
      4     j = i * n
```
> 可以向前向后跳转
- unt(il) [lineno]
> 与next的区别在于，当处于循环中时，next继续执行下一次循环，until则执行完整个循环
- run [args...]
```
run --name abc
==
python demo.py --name abc
```
- retval
```
None
> d:\markdown\ipdb\demo.py(11)f()
     10         print(i, j)
---> 11     return
     12 

ipdb> rv
None
```
- source expression
```
ipdb> ll
      1 import pdb
      2 
      3 def calc(i, n):
      4     j = i * n
      5     return j
      6 
      7 def f(n):
      8     for i in range(n):
      9         j = calc(i, n)
     10         print(i, j)
     11     return
     12 
     13 if __name__ == '__main__':
14-> 14     f(5)

ipdb> source f
  7     def f(n):
  8         for i in range(n):
  9             j = calc(i, n)
 10             print(i, j)
 11         return
 ```
 - display [expression]
 ```
16    9         j = calc(i, n)
---> 10         print(i, j)
     11     return

ipdb> display j
display j: 0
ipdb> c
0 0
> d:\markdown\ipdb\demo.py(9)f()
      8     for i in range(n):
16--> 9         j = calc(i, n)
     10         print(i, j)

ipdb> c
1 5
> d:\markdown\ipdb\demo.py(9)f()
      8     for i in range(n):
16--> 9         j = calc(i, n)
     10         print(i, j)

display j: 5  [old: 0]
ipdb> c
2 10
> d:\markdown\ipdb\demo.py(9)f()
      8     for i in range(n):
16--> 9         j = calc(i, n)
     10         print(i, j)

display j: 10  [old: 5]
 ```
> 打断点后，对变量设置display，每当变量值改变，显示新值与旧值 
- ignore bpnumber [count]
> 忽略count次断点，count默认为0

- interact
> 使用<kbd>ctrl</kbd> + <kbd>z</kbd> 或 quit()退出
> 当在jupyter notebook中进入交互模式后，退出方式很糟心
>> - Linux系统下，在terminal输入
>>    `echo '\x04' | xclip -selection clipboard`
>> - 任何平台下，另开一个python terminal输入
      `from pandas.io.clipboard import copy; copy("\x04")`
       
       再粘贴至ipdb的交互窗口，Enter即可退出interact模式

> 原理：[This puts the ASCII character 0x04, "END OF TRANSMISSION", onto your clipboard. This character is a "control character" that signals that there is no more input, which causes the PDB interactive session to end.][1]

---
[^_^]: 链接

[1]:https://stackoverflow.com/questions/47522316/exit-pdb-interactive-mode-from-jupyter-notebook