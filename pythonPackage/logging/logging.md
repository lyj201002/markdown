# logging
---
## TLDR
```python
import logging

logging.basicConfig(filename=r'logging.log', level=logging.DEBUG)
logging.critical('It is a critical level info!')
logging.error('It is a error level info!')
logging.warning('It is a warning level info!')
logging.info('It is a info level info!')
logging.debug('It is a debug level info!')
```
## 
![logging](./pic1 "logging 框架流程")

日志记录框架可分为如下部分： 
- Logger：进行日志记录时创建的对象，可以调用他的方法传入日志模板和信息生成一条条日志记录，称作Log Record
- Log Record：日志记录
- Handler：用来处理日志记录的类，将日志输出到指定位置和储存形式，如可以指定将日志通过FTP协议记录到远程服务器上
- Filter：过滤想留下的日志
- Parent Handler：Handler间可以存在分层关系，使得不同Handler间共享相同功能的代码
## Level
Level|Numeric value
:-:|:-:
CRITICAL|50
ERROR|40
WARNING|30
INFO|20
DEBUG|10
NOTSET|0
## Usage
``` python
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

logger.info('This is a log info')
logger.debug('Debugging')
logger.warning('Warning exists')
logger.info('Finish')
```
由于logger配置为INFO，所以只输出高于INFO级别的日志， DEBUG级别的日志贝过滤掉
```
2018-06-03 13:42:43,526 - __main__ - INFO - This is a log info
2018-06-03 13:42:43,526 - __main__ - WARNING - Warning exists
2018-06-03 13:42:43,526 - __main__ - INFO - Finish
```
---
**logging.basicConfig(\*\*kwargs)**  
Does basic configuration for the logging system by creating a StreamHandler with a default Formatter and adding it to the root logger. The functions debug(), info(), warning(), error() and critical() will call basicConfig() automatically if no handlers are defined for the root logger.  
The following keyword arguments are supported.

Format|Description
-|-
filename|Specifies that a FileHandler be created, using the specified filename, rather than a StreamHandler.（输出到文件）
filemode|If filename is specified, open the file in this mode. Defaults to 'a'.（'a' 日志为追加模式，'w'为重写模式）
format|Use the specified format string for the handler. Defaults to attributes levelname, name and message separated by colons.（日志输出样式）
datefmt|Use the specified date/time format, as accepted by time.strftime().（时间样式）
style|If format is specified, use this style for the format string. One of '%', '{' or '$' for printf-style, str.format() or string.Template respectively. Defaults to '%'.（格式化字符串表示符）
level|Set the root logger level to the specified level.（输出Level）
stream|Use the specified stream to initialize the StreamHandler. Note that this argument is incompatible with filename - if both are present, a ValueError is raised.
handlers|If specified, this should be an iterable of already created handlers to add to the root logger. Any handlers which don’t already have a formatter set will be assigned the default formatter created in this function. Note that this argument is incompatible with filename or stream - if both are present, a ValueError is raised.
force|If this keyword argument is specified as true, any existing handlers attached to the root logger are removed and closed, before carrying out the configuration as specified by the other arguments.
encoding|If this keyword argument is specified along with filename, its value is used when the FileHandler is created, and thus used when opening the output file.
errors|If this keyword argument is specified along with filename, its value is used when the FileHandler is created, and thus used when opening the output file. If not specified, the value ‘backslashreplace’ is used. Note that if None is specified, it will be passed as such to func:open, which means that it will be treated the same as passing ‘errors’.

其中 format部分参数如下  
- %(name) s: logger名称
- %(asctime) s：打印日志的时间。
- %(message) s：打印日志信息。
- %(lineno) d：打印日志的当前行号。
- %(levelno) s：打印日志级别的数值。
- %(process) d：打印进程 ID。
- %(processName) s：打印线程名称。
- %(module) s：打印模块名称。
- %(levelname) s：打印日志级别的名称。
- %(pathname) s：打印当前执行程序的路径，其实就是 sys.argv [0]。
- %(filename) s：打印当前执行程序名。
- %(funcName) s：打印日志的当前函数。
- %(thread) d：打印线程 ID。
- %(threadName) s：打印线程名称。

**Handler**

```python

import logging

logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)
handler = logging.FileHandler('output.log')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

logger.info('This is a log info')
logger.debug('Debugging')
logger.warning('Warning exists')
logger.info('Finish')
```
---
```mermaid
graph LR
A[getlogger] -->B[setlevel] -->C[addHandler] -->G[INFO]
D[Handler] --> E[setFormatter] -->C
F[Formatter] -->E
```

**Traceback**
```python
try:
    result = 5 / 0
except Exception as e:
    # bad
    logging.error('Error: %s', e)
    # good
    logging.error('Error', exc_info=True)
    # good
    logging.exception('Error')
```
**配置共享**

先定义一个main.py文件为父文件
```python 
import logging
import core

logger = logging.getLogger('main')
logger.setLevel(level=logging.DEBUG)

# Handler
handler = logging.FileHandler('result.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

logger.info('Main Info')
logger.debug('Main Debug')
logger.error('Main Error')
core.run()

```

core.py文件如下
```python 
import logging

logger = logging.getLogger('main.core')

def run():
    logger.info('Core Info')
    logger.debug('Core Debug')
    logger.error('Core Error')
```
> 子文件中logger名为main.core，即父文件logger名称.***，即可复用父文件中logger配置。结果如下
```bash

2018-06-03 16:55:56,259 - main - INFO - Main Info
2018-06-03 16:55:56,259 - main - ERROR - Main Error
2018-06-03 16:55:56,259 - main.core - INFO - Core Info
2018-06-03 16:55:56,259 - main.core - ERROR - Core Error
```
**文件配置**

在开发过程中，将配置在代码里面写死并不是一个好的习惯，更好的做法是将配置写在配置文件里面，我们可以将配置写入到配置文件，然后运行时读取配置文件里面的配置，这样是更方便管理和维护的，下面我们以一个实例来说明一下，首先我们定义一个 yaml 配置文件：
```yaml
version: 1
formatters:
  brief:
    format: "%(asctime)s - %(message)s"
  simple:
    format: "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
handlers:
  console:
    class : logging.StreamHandler
    formatter: brief
    level   : INFO
    stream  : ext://sys.stdout
  file:
    class : logging.FileHandler
    formatter: simple
    level: DEBUG
    filename: debug.log
  error:
    class: logging.handlers.RotatingFileHandler
    level: ERROR
    formatter: simple
    filename: error.log
    maxBytes: 10485760
    backupCount: 20
    encoding: utf8
loggers:
  main.core:
    level: DEBUG
    handlers: [console, file, error]
root:
  level: DEBUG
  handlers: [console]
```
这里我们定义了 formatters、handlers、loggers、root 等模块，实际上对应的就是各个 Formatter、Handler、Logger 的配置，参数和它们的构造方法都是相同的。 接下来我们定义一个主入口文件，main.py，内容如下：
```python

import logging
import core
import yaml
import logging.config
import os


def setup_logging(default_path='config.yaml', default_level=logging.INFO):
    path = default_path
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            config = yaml.load(f)
            logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def log():
    logging.debug('Start')
    logging.info('Exec')
    logging.info('Finished')


if __name__ == '__main__':
    yaml_path = 'config.yaml'
    setup_logging(yaml_path)
    log()
    core.run()
```
这里我们定义了一个 setup_logging () 方法，里面读取了 yaml 文件的配置，然后通过 dictConfig () 方法将配置项传给了 logging 模块进行全局初始化。 另外这个模块还引入了另外一个模块 core，所以我们定义 core.py 如下：
```python

import logging

logger = logging.getLogger('main.core')

def run():
    logger.info('Core Info')
    logger.debug('Core Debug')
    logger.error('Core Error')
```
这个文件的内容和上文是没有什么变化的。 观察配置文件，主入口文件 main.py 实际上对应的是 root 一项配置，它指定了 handlers 是 console，即只输出到控制台。另外在 loggers 一项配置里面，我们定义了 main.core 模块，handlers 是 console、file、error 三项，即输出到控制台、输出到普通文件和回滚文件。 这样运行之后，我们便可以看到所有的运行结果输出到了控制台：
```bash
2018-06-03 17:07:12,727 - Exec
2018-06-03 17:07:12,727 - Finished
2018-06-03 17:07:12,727 - Core Info
2018-06-03 17:07:12,727 - Core Info
2018-06-03 17:07:12,728 - Core Error
2018-06-03 17:07:12,728 - Core Error

```
在 debug.log 文件中则包含了 core.py 的运行结果：

```bash
2018-06-03 17:07:12,727 - main.core - INFO - Core Info
2018-06-03 17:07:12,727 - main.core - DEBUG - Core Debug
2018-06-03 17:07:12,728 - main.core - ERROR - Core Error

```

**logging.config.dictConfig(config)**
接收的字典必须含有项`version: 1`，其他可选择项有
- formatters
    - format
    - datefmt: default None
- filters
- handlers
    - class (mandatory). This is the fully qualified name of the handler class.
    - level (optional). The level of the handler.
    - formatter (optional). The id of the formatter for this handler.
    - filters (optional). A list of ids of the filters for this handler.
    >
    ``` 
    handlers:
    console:
    class : logging.StreamHandler
    formatter: brief
    level   : INFO
    filters: [allow_foo]
    stream  : ext://sys.stdout 
  file:
    class : logging.handlers.RotatingFileHandler
    formatter: precise
    filename: logconfig.log
    maxBytes: 1024
    backupCount: 3
    ```
    > **ext://sys.stdout 中ext://为Access to external objects， 为使在yaml中外部python库不被识别为字符串**
- loggers
    - level (optional). The level of the logger.
    - propagate (optional). The propagation setting of the logger.
    - filters (optional). A list of ids of the filters for this logger.
    - handlers (optional). A list of ids of the handlers for this logger.

- root 
    > the configuration for the root logger
- ...

